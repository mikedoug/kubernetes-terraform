SOFTWARE REQUIRED
=================
* [Terraform](terraform.io)
* bash & scp (Cygwin under Windows)


ENVIRONMENT VARIABLE CONFIGURATION
==================================

  * TF_VAR_do_api_token - Set this to your Digital Ocean API token.
  * TF_VAR_do_private_key_hash - Set this to the hash code for your SSH private key on Digital Ocean.
  * TF_VAR_do_private_key - The path to your private key
  * TF_VAR_bash_do_private_key - The path to your private key

If you are running on Windows:
  * TF_VAR_do_private_key needs to be the Windows format path (e.g. C:\users\mikedoug\...)
  * TF_VAR_bash_do_private_key needs to be the Cygwin format (e.g. /cygdrive/c/users/mikedoug/...)


CONFIGURE NUMBER OF NODES
=========================
In kubernetes.tf, there is a count=1 line inside of the kube-node definition.  Increase that number to get more nodes generated.


GETTING THINGS DONE
===================
Now, to spin the environment up: `terraform apply`.
If you change count, you can do the `terraform apply` again and it will add or remove nodes as needed.
Lastly, you can destroy all of the VMs with: `terraform destroy`.