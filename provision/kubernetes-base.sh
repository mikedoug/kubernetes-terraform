# Execute on both nodes
apt-get update && apt-get upgrade -y
apt-get update
apt-get install -y docker.io apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl

# This is assuming cgroupfs because that's what Ubuntu 16.04 currently does.
sed -i '${s/$/ --cgroup-driver=cgroupfs/}' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

systemctl daemon-reload
systemctl restart kubelet

echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> /root/.bashrc