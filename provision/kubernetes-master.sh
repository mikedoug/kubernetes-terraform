kubeadm init --pod-network-cidr=10.244.0.0/16
export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.9.1/Documentation/kube-flannel.yml

# kubeadm join 167.99.239.32:6443 --token 74vhw9.vg0pw1zqmoofti6y --discovery-token-ca-cert-hash sha256:11c564d501e69f479539f9b56db4410d46562b74dd696bb1cd39b49305a92737

mkdir -p /root/kube_terraform
(
  cd /root/kube_terraform
  kubeadm token create > token
  openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* /sha256:/' > discovery-token-ca-cert-hash
  kubeadm config view | fgrep "advertiseAddress" | sed -e 's/.*: *//' > api-address
  kubeadm config view | fgrep "bindPort" | sed -e 's/.*: *//' > api-port
)