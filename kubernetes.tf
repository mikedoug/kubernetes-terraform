variable "do_api_token" {}
variable "do_private_key_hash" {}
variable "do_private_key" {}
variable "bash_do_private_key" {}

provider "digitalocean" {
  token = "${var.do_api_token}"
}

resource "digitalocean_droplet" "kube-master" {
  image    = "ubuntu-16-04-x64"
  name     = "demo-kube-master"
  region   = "nyc1"
  size     = "2gb"
  ssh_keys = ["${var.do_private_key_hash}"]

  # We will later pull down a set of variables from the kube-master, but first remove any stale versions of them.
  provisioner "local-exec" {
    command     = "/bin/rm -rf .kube_terraform"
    interpreter = ["bash", "-c"]
  }

  # Perform our primary provisioning
  provisioner "remote-exec" {
    scripts = [
      "provision/kubernetes-base.sh",
      "provision/kubernetes-master.sh",
    ]

    connection {
      type        = "ssh"
      user        = "root"
      private_key = "${file(var.do_private_key)}"
    }
  }

  # Retrieve our variables from the kube-master.  These will be used by kube-nodes for registration.
  provisioner "local-exec" {
    command     = "scp -r -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ${var.bash_do_private_key} root@${self.ipv4_address}:kube_terraform/ .kube_terraform"
    interpreter = ["bash", "-c"]
  }
}

resource "digitalocean_droplet" "kube-node" {
  image    = "ubuntu-16-04-x64"
  name     = "demo-kube-node-${count.index}"
  region   = "nyc1"
  size     = "2gb"
  ssh_keys = ["${var.do_private_key_hash}"]
  count    = 2

  depends_on = [
    "digitalocean_droplet.kube-master",
  ]

  # Perform our base kubernetes provisioning
  provisioner "remote-exec" {
    scripts = [
      "provision/kubernetes-base.sh",
    ]

    connection {
      type        = "ssh"
      user        = "root"
      private_key = "${file(var.do_private_key)}"
    }
  }

  # Push our kube-master variables to the node
  provisioner "local-exec" {
    command     = "scp -r -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ${var.bash_do_private_key} .kube_terraform root@${self.ipv4_address}:kube_terraform/"
    interpreter = ["bash", "-c"]
  }

  # Using the kube-master variables, join the cluster
  provisioner "remote-exec" {
    inline = [
      "kubeadm join $(cat /root/kube_terraform/api-address):$(cat /root/kube_terraform/api-port) --token $(cat /root/kube_terraform/token) --discovery-token-ca-cert-hash $(cat /root/kube_terraform/discovery-token-ca-cert-hash)",
    ]

    connection {
      type        = "ssh"
      user        = "root"
      private_key = "${file(var.do_private_key)}"
    }
  }
}